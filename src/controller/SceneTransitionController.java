package controller;

import java.io.IOException;
import java.io.Serializable;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import model.User;
import model.Album;
import model.Photo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Optional;

/**
 * The controller class that actually handles transitioning between views/controllers within
 * the program and stores the created Users, the selected Album, and the selected Photo.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 *
 */
public class SceneTransitionController implements Serializable {
	
	/** The serial version id for the SceneTransitionController class. */
	private static final long serialVersionUID = 7780711664501846960L;
	
	/** The Stage used to display our application's views.*/
	public transient static Stage mainStage;
	
	/** The date format used to display the earliest and latest date of a Photo within an Album and
	 * also the imageDate of a Photo.*/
	public static final String dateFormat = "MM/dd/yyyy HH:mm:ss";
	
	/** The Calendar used to parse, display, and compare the dates associated with
	 * our Photos.*/
	public transient static Calendar calendar;
	
	/** The ArrayList of all non-admin/non-stock users added to the program.*/
	public ArrayList<User> nonAdminUsers = new ArrayList<User>();	
	
	/** The 'stock' user. Starts out with a 'stock' album containing the Photos in the
	 * data/assets directory*/
	public User stockUser;
	
	/** The currently logged in User.*/
	public transient User loggedInUser;
	
	/** The currently selected Album.*/
	public transient Album selectedAlbum;
	
	/** The currently selected Photo.*/
	public transient Photo selectedPhoto;
	
	/**
	 * Delete the non-stock/non-admin User corresponding to the username
	 * provided in the parameter. 
	 *  
	 * @param userName The String username of the User we wish to delete.
	 */
	public void deleteUser(String userName) {
		for(User currentUser : nonAdminUsers) {
			if(currentUser.userName.equalsIgnoreCase(userName)) {
				nonAdminUsers.remove(currentUser);
				return;
			}
		}
	}
	
	/**
	 * Create the calendar used to parse, display, and compare the dates of our Photos.
	 * If the 'stock' User has not yet been set up, sets it up.
	 */
	public void initialize() {
		calendar = Calendar.getInstance();
		calendar.set(Calendar.MILLISECOND, 0);
		if(stockUser == null) {
			this.setupStockUser();
		}
	}
	
	/**
	 * Sets up the 'stock' User, including a 'stock' Album
	 * containing all the Photos in the data/assets directory.
	 */
	public void setupStockUser() {
		stockUser = new User("Stock");
		stockUser.addAlbum(Album.buildStockAlbum());
	}
	
	/**
	 * Determines whether a non-stock / non-admin User exists with
	 * the provided username String. 
	 * 
	 * @param userName The username String to check our existing non-stock / non-admin
	 * users against.
	 * 
	 * @return true if a non-admin / non-stock User exists with the provided username, false otherwise. 
	 */
	public boolean userExistsWithName(String userName) {
		for(User currentUser : nonAdminUsers) {
			if(currentUser.userName.equalsIgnoreCase(userName)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Gets the non-stock/non-admin User corresponding to the provided username String.
	 * 
	 * @param userName The username String corresponding to the User
	 * object we are trying to get.
	 * 
	 * @return The User object that corresponds to the provided username String
	 * or null if no such User exists.
	 */
	public User getUser(String userName) {
		for(User currentUser : nonAdminUsers) {
			if(currentUser.userName.equalsIgnoreCase(userName)) {
				return currentUser;
			}
		}
		
		return null;
	}
	
	/**
	 * Sets the mainStage variable used for displaying our views.
	 * 
	 * @param stage The Stage to set our mainStage variable to.
	 */
	public void setStage(Stage stage) {
		mainStage = stage;
	}
	
	/**
	 * Transition scenes between the current view/controller to the one provided.
	 * 
	 * @param controller The controller to use when transitioning from the 
	 * current view/controller.
	 */
	public void transitionScenesWithNewController (PhotosController controller) {
		mainStage.setTitle(controller.viewTitle());
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(controller.fxmlPath()));
			AnchorPane pane = (AnchorPane)loader.load();
			
			controller = loader.getController(); // controller is defined
			controller.setMainStage(mainStage);
			
			Scene scene = new Scene(pane);
			mainStage.setScene(scene);
			mainStage.setResizable(false);
			mainStage.show();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Load the Login view / LoginController.
	 */
	public void loadLogin() {
		this.transitionScenesWithNewController(new LoginController());
	}
	
	/**
	 * Transition from the Login view / LoginController to the Admin view / AdminController.
	 */
	public void transitionFromLoginToAdmin() {
		this.transitionScenesWithNewController(new AdminController());
	}
	
	/**
	 * Transition from the Login view / LoginController to the AlbumManagement view / 
	 * AlbumManagementController, setting the loggedInUser along the way. 
	 * 
	 * @param userName The string userName to log in as.
	 */
	public void transitionFromLoginToAlbumManagementWithUserName(String userName) {
		
		if(userName.equalsIgnoreCase(stockUser.userName)) {
			this.loggedInUser = stockUser;
		} else {
			this.loggedInUser = this.getUser(userName);			
		}

		this.transitionScenesWithNewController(new AlbumManagementController());
	}
	
	/**
	 * Transition from the AlbumManagement view / AlbumManagementController to the
	 * PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromAlbumManagementToPhotoManagement() {
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Transition from the PhotoManagement view / PhotoManagementController to the
	 * AlbumManagement view / AlbumManagementController.
	 */
	public void transitionFromPhotoManagementToAlbumManagement() {
		this.transitionScenesWithNewController(new AlbumManagementController());
	}
	
	/**
	 * Transition from the PhotoManagement view / PhotoManagementController to the
	 * ViewPhoto view / ViewPhotoController.
	 */
	public void transitionFromPhotoManagementToViewPhoto() {
		this.transitionScenesWithNewController(new ViewPhotoController());
	}
	
	/**
	 * Transition from the ViewPhoto view / ViewPhotoController to the
	 * PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromViewPhotoToPhotoManagement() {
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Transition from the PhotoManagement view / PhotoManagementController to the
	 * AddPhoto view / AddPhotoController.
	 */
	public void transitionFromPhotoManagementToAddPhoto() {
		this.transitionScenesWithNewController(new AddPhotoController());
	}
	
	/**
	 * Transition from the AddPhoto view / AddPhotoController to the
	 * PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromAddPhotoToPhotoManagement() {		
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Transition from the PhotoManagement view / PhotoManagementController to the
	 * CreateAlbum view / CreateAlbumController.
	 */
	public void transitionFromPhotoManagementToCreateAlbum() {
		this.transitionScenesWithNewController(new CreateAlbumController());
	}
	
	/**
	 * Transition from the CreateAlbum view / CreateAlbumController to the
	 * PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromCreateAlbumToPhotoManagement() {
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Transition from the PhotoManagement view / PhotoManagementController to the
	 * AddTag view / AddTagController.
	 */
	public void transitionFromPhotoManagementToAddTag() {
		this.transitionScenesWithNewController(new AddTagController());
	}
	
	/**
	 * Transition from the AddTag view / AddTagController to the
	 * PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromAddTagToPhotoManagement() {
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Transition from the PhotoManagement view / PhotoManagementController to the
	 * RemoveTag view / RemoveTagController.
	 */
	public void transitionFromPhotoManagementToRemoveTag() {
		this.transitionScenesWithNewController(new RemoveTagController());
	}
	
	/**
	 * Transition from the RemoveTag view / RemoveTagController to the
	 * PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromRemoveTagToPhotoManagement() {
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Transition from the PhotoManagement view / PhotoManagementController to the
	 * CopyPhoto view / CopyPhotoController.
	 */
	public void transitionFromPhotoManagementToCopyPhoto() {	
		this.transitionScenesWithNewController(new CopyPhotoToAlbumController());
	}
	
	/**
	 * Transition from the CopyPhoto view / CopyPhotoController to the
	 * PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromCopyPhotoToPhotoManagement() {		
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Transition from the PhotoManagement view / PhotoManagementController to the
	 * MovePhoto view / MovePhotoController.
	 */
	public void transitionFromPhotoManagementToMovePhoto() {
		this.transitionScenesWithNewController(new MovePhotoToAlbumController());
	}

	/**
	 * Transition from the MovePhoto view / MovePhotoController to the
	 * PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromMovePhotoToPhotoManagement() {
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Transition from the AlbumManagement view / AlbumManagementController to the
	 * ConstructDateRangeSearch view / ConstructDateRangeSearchController.
	 */
	public void transitionFromAlbumManagementToDateRangeSearch() {
		this.transitionScenesWithNewController(new ConstructDateRangeSearchController());
	}

	/**
	 * Transition from the ConstructDateRangeSearch view / ConstructDateRangeSearchController 
	 * to the AlbumManagement view / AlbumManagementController.
	 */
	public void transitionFromDateRangeSearchToAlbumManagement() {
		this.transitionScenesWithNewController(new AlbumManagementController());
	}
	
	/**
	 * Transition from the ConstructDateRangeSearch view / ConstructDateRangeSearchController 
	 * to the PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromDateRangeSearchToPhotoManagement() {
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Transition from the AlbumManagement view / AlbumManagementController 
	 * to the ConstructTagSearch view / ConstructTagSearchController.
	 */
	public void transitionFromAlbumManagementToTagSearch() {
		this.transitionScenesWithNewController(new ConstructTagSearchController());
	}
	
	/**
	 * Transition from the ConstructTagSearch view / ConstructTagSearchController 
	 * to the AlbumManagement view / AlbumManagementController.
	 */
	public void transitionFromTagSearchToAlbumManagement() {
		this.transitionScenesWithNewController(new AlbumManagementController());
	}

	/**
	 * Transition from the ConstructTagSearch view / ConstructTagSearchController 
	 * to the PhotoManagement view / PhotoManagementController.
	 */
	public void transitionFromTagSearchToPhotoManagement() {
		this.transitionScenesWithNewController(new PhotoManagementController());
	}
	
	/**
	 * Clears the logged in User, the selected Album, and the selected Photo.
	 * Tries to save the app's data and returns the User to the Login view / LoginController.  
	 */
	public void logout() {
		Alert confirmation = new Alert(AlertType.CONFIRMATION);
		confirmation.initOwner(mainStage); 
		confirmation.setTitle("Log Out");
		confirmation.setHeaderText("Are you sure you want to log out?");
		Optional<ButtonType> result = confirmation.showAndWait();

		if (result.get() == ButtonType.OK){
			this.loggedInUser = null;
			this.selectedAlbum = null;
			this.selectedPhoto = null;
			
			try {
				Photos.writeAppState(this);
			} catch (IOException e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(mainStage); 
				alert.setTitle("Error Saving Data!");
				alert.setHeaderText("An error has occurred while saving your data!\n Error info is listed below:");
				alert.setContentText(e.toString());
				alert.showAndWait();
			}
			
			this.loadLogin();
		}
		
	}
}
