package controller;

import java.io.File;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import model.Tag;

/**
 * The controller class for the ViewPhoto view which allows the User to see a more detailed
 * view of the selected Photo.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class ViewPhotoController extends PhotosController {
	
	/** The Text used to display the selected Photo's filename.*/
	@FXML
	private Text photoFileNameText;
	
	/** The ImageView used to display the selected Photo.*/
	@FXML
	private ImageView photoImageView;
	
	/** The TextField used to display the location the selected Photo is tagged with. This is not editable in this screen.*/
	@FXML
	private TextField taggedLocationTextField;
	
	/** The TextField used to display the caption of the selected Photo. This is not editable in this screen.*/
	@FXML
	private TextField photoCaptionTextField;

	/** The ListView used to display the person Tags the selected Photo is tagged with.*/
	@FXML
	private ListView<Tag> taggedPeopleListView;
	
	/** The Button that returns the user to the PhotoManagement view / PhotoManagementController.*/
	@FXML
	private Button returnToPhotoManagementButton;
	
	/** The Text used to display the imageDate of the selected Photo.*/
	@FXML
	private Text imageDateValueText;
	
	@Override
	public String fxmlPath() { 
		return "../view/ViewPhoto.fxml";	
	}

	@Override
	public String viewTitle() {
		return "View Photo";
	}
	
	/**
	 * On load, confirms the selected Photo actually exists and if it does,
	 * displays it and its info on the screen. Otherwise warns the User that it
	 * may have been moved or deleted. 
	 */
	@FXML
	public void initialize() {
		File file = new File(Photos.transitionController.selectedPhoto.path);
    	
    	if(file.exists()) {
        	
    		photoImageView.setImage(new Image(file.toURI().toString()));
    	    photoFileNameText.setText(Photos.transitionController.selectedPhoto.getOnlyFileNameAndExtension());
    		photoCaptionTextField.setText(Photos.transitionController.selectedPhoto.caption);
    		taggedPeopleListView.setItems(FXCollections.observableArrayList(Photos.transitionController.selectedPhoto.taggedPersons));
    		imageDateValueText.setText(Photos.transitionController.selectedPhoto.imageDateString());
    		if(Photos.transitionController.selectedPhoto.hasLocationTag()) {
        		taggedLocationTextField.setText(Photos.transitionController.selectedPhoto.locationTag.toString());    			
    		}
    	} else {
			Alert alert = new Alert(AlertType.WARNING);
			alert.initOwner(mainStage); 
			alert.setTitle("Selected Photo Not Found!");
			alert.setHeaderText(Photos.transitionController.selectedPhoto.getOnlyFileNameAndExtension() + " not found!");
			alert.setContentText(Photos.transitionController.selectedPhoto.path + " does not appear to correspond to an actual image on disk. This file may have been moved or deleted.");
			alert.showAndWait();
    	}
	}
	
	/**
	 * Returns the user to the PhotoManagement view / PhotoManagementController.
	 */
	public void returnToPhotoManagementAction() {
		Photos.transitionController.transitionFromViewPhotoToPhotoManagement();
	}
}
