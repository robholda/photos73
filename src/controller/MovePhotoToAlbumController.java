package controller;

import java.util.Optional;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Alert.AlertType;
import model.Album;

/**
 * The controller class for the MovePhotoToAlbum view. Allows the 
 * User to select an Album to move the selected Photo to.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class MovePhotoToAlbumController extends PhotosController {
	
	/** The ComboBox where all of the logged in User's Albums are listed.*/
	@FXML
	private ComboBox<Album> userAlbumsComboBox;
	
	/** The Button that triggers actually moving the Photo to the User selected Album.*/
	@FXML
	private Button movePhotoButton;
	
	/** The Button that cancels moving the Photo, returning the User to the PhotoManagement view /
	 * PhotoManagementController. */
	@FXML
	private Button cancelButton;
	
	@Override
	public String fxmlPath() { 
		return "../view/MovePhotoToAlbum.fxml";
	}

	@Override
	public String viewTitle() {
		return "Move Photo To Album";
	}
	
	/**
	 * On load, populate the userAlbumsComboBox with the logged in User's Albums and select the
	 * Album the selected Photo is currently a part of.
	 */
	@FXML
	public void initialize() {
		userAlbumsComboBox.setItems(FXCollections.observableArrayList(Photos.transitionController.loggedInUser.userAlbums));
		userAlbumsComboBox.getSelectionModel().select(Photos.transitionController.selectedAlbum);
	}
	
	/**
	 * Moves the selected Photo to the Album specified by the User. This will fail if either of
	 * the following is true:<br>
	 * -The selected Album is the same Album the selected Photo is currently a part of<br>
	 * -The selected Album already contains the selected Photo
	 */
	public void movePhotoAction() {
		if(userAlbumsComboBox.getSelectionModel().getSelectedItem().equals(Photos.transitionController.selectedAlbum)) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Moving Photo!");
			alert.setHeaderText("Cannot move a Photo to the Album you are moving it from!");
			alert.setContentText("Please select a different Album and try again.");
			alert.showAndWait();
		} else {
			Alert confirmation = new Alert(AlertType.CONFIRMATION);
			confirmation.initOwner(mainStage); 
			confirmation.setTitle("Move Photo");
			confirmation.setHeaderText("Are you sure you want to move this Photo?");
			Optional<ButtonType> result = confirmation.showAndWait();

			if (result.get() == ButtonType.OK){
				if(!Photos.transitionController.loggedInUser.getAlbum(userAlbumsComboBox.getSelectionModel().getSelectedItem().albumName).photoExistsInAlbumWithName(Photos.transitionController.selectedPhoto.path)) {
					if(Photos.transitionController.loggedInUser.getAlbum(userAlbumsComboBox.getSelectionModel().getSelectedItem().albumName).addPhoto(Photos.transitionController.selectedPhoto)) {
						Photos.transitionController.selectedAlbum.removePhoto(Photos.transitionController.selectedPhoto.path);
						
						Photos.transitionController.selectedPhoto = null;
						Photos.transitionController.transitionFromMovePhotoToPhotoManagement();
						
					}	
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.initOwner(mainStage); 
					alert.setTitle("Error Moving Photo!");
					alert.setHeaderText("Cannot move a Photo to an Album the same Photo already exists in!");
					alert.setContentText("This Album already contains the Photo you are moving. Please select a different Album and try again.");
					alert.showAndWait();
				}	
			}
		}
	}
	
	/**
	 * Backs out of the MovePhotoToAlbum view / MovePhotoToAlbumController, 
	 * returning the User to the PhotoManagement view / PhotoManagementController.
	 */
	public void cancelAction() {
		Photos.transitionController.transitionFromMovePhotoToPhotoManagement();
	}
}
