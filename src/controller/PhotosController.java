package controller;

import javafx.stage.Stage;

/**
 * The abstract parent class of all our controller classes (not including the main Photos class).
 * Contains a Stage member for presenting this controller and its view as well as abstract functions
 * to get the title of this controller's view and this controller's view's fxml file's path 
 * (overridden by each child class of this class). 
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public abstract class PhotosController {
	/** The Stage used to present this controller and it's view*/
	Stage mainStage;
	
	/**
	 * Set this controller's mainStage member.
	 * 
	 * @param stage The Stage object to set this controller's mainStage member to.
	 */
	public void setMainStage(Stage stage) {
		mainStage = stage;
	}
	
	/**
	 * Get the path of the fxml file corresponding to this controller's view.
	 * 
	 * @return The String path of the fxml file for this controller's view.
	 */
	public abstract String fxmlPath();

	/**
	 * Get the title of this controller's view.
	 * 
	 * @return The String title of this controller's view.
	 */
	public abstract String viewTitle();
}
