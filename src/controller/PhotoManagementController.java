package controller;

import java.io.File;
import java.util.Optional;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.Photo;
import model.Tag;

/**
 * The controller class for the PhotoManagement view. Allows the 
 * User to:<br>
 * -Add new Photos<br>
 * -Remove the selected Photo<br>
 * -Copy the selected Photo to a different Album<br>
 * -Move the selected Photo to a different Album<br>
 * -View the selected Photo<br>
 * -Create a new Album with the Photos displayed on this screen<br>
 * -Update the caption of the selected Photo<br>
 * -Add a Tag to the selected Photo<br>
 * -Remove a Tag from the selected Photo<br>
 * -Preview the selected Photo<br>
 * -Navigate to the next and previous Photo in the Photo list<br>
 * -Log Out<br>
 * -Return to the Album Management screen.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class PhotoManagementController extends PhotosController {
	
	/** The ListView that displays a list of all the Photos in the currently selected Album.*/
	@FXML
	private ListView<Photo> photosInAlbumListView;
	
	/** The ListView that displays a list of all the people tagged in the currently 
	 * selected Photo.*/
	@FXML
	private ListView<Tag> peopleTaggedInCurrentPhotoListView;
	
	/** The TextField that displays the caption of the selected Photo when one is selected 
	 * and allows the user to update the caption of the selected Photo.*/
	@FXML
	private TextField selectedPhotoCaptionTextField;
	
	/** The TextField that displays the tagged location of the selected Photo.
	 * This value is not directly editable here and must be edited in the RemoveTagController.*/	
	@FXML
	private TextField selectedPhotoLocationTagTextField;

	/** The ImageView where the selected Photo is displayed if one is selected.*/
	@FXML
	private ImageView currentPhotoImageView;
	
	/** The Button that triggers the AddPhoto view / AddPhotoController, allowing the user
	 * to add a new Photo to the selected Album.*/
	@FXML
	private Button addNewPhotoButton;
	
	/** The Button that allows the user to remove the selected Photo from the selected Album.*/
	@FXML
	private Button removeSelectedPhotoButton;
	
	/** The Button that triggers the CopyPhotoToAlbum view / CopyPhotoToAlbumController, 
	 * allowing the user to copy the selected Photo to a different Album.*/
	@FXML
	private Button copySelectedPhotoToNewAlbumButton;
	
	/** The Button that triggers the MovePhotoToAlbum view / MovePhotoToAlbumController, 
	 * allowing the user to move the selected Photo to a different Album.*/
	@FXML
	private Button moveSelectedPhotoToNewAlbumButton;
	
	/** The Button that triggers the ViewPhoto view / ViewPhotoController, allowing the user to
	 * see a larger view of the selected Photo as well as that Photo's info.*/
	@FXML
	private Button viewSelectedPhotoButton;
	
	/** The Button that triggers the CreateAlbum view / CreateAlbumController, allowing the user to
	 * create a new Album with a provided Album name from the list of Photos displayed on this 
	 * screen.*/
	@FXML
	private Button createNewAlbumButton;
	
	/** The Button that allows the User to update the selected Photo's caption.*/
	@FXML
	private Button updateCaptionButton;
	
	/** The Button that triggers the AddTag view / AddTagController, allowing the user to
	 * add a Tag to the selected Photo.*/
	@FXML
	private Button addTagToSelectedPhotoButton;

	/** The Button that triggers the RemoveTag view / RemoveTagController, allowing the user to
	 * remove a Tag to the selected Photo.*/
	@FXML
	private Button removeTagFromSelectedPhotoButton;
	
	/** The Button that selects the previous Photo in the list based on the currently selected
	 * Photo. If the first Photo in the list is selected, does nothing. If no Photo is selected,
	 * does nothing.*/
	@FXML
	private Button selectPreviousPhotoButton;

	/** The Button that selects the next Photo in the list based on the currently selected
	 * Photo. If the last Photo in the list is selected, does nothing. If no Photo is selected,
	 * selects the first Photo in the list.*/
	@FXML
	private Button selectNextPhotoButton;
	
	/** The Button that returns the user to the AlbumManagement view / AlbumManagementController.*/
	@FXML
	private Button returnToAlbumManagementButton;
	
	/** The Button used to trigger logging the current User out.*/
	@FXML
	private Button logoutButton;
	
	@Override
	public String fxmlPath() { 
		return "../view/PhotoManagement.fxml";	
	}

	@Override
	public String viewTitle() {
		return "Photo Management";
	}
	
	/**
	 * On load:<br> 
	 * -populates the photosInAlbumListView with the Photos in the selected Album<br>
	 * -sets the way the photosInAlbumListView builds cells for display (with a preview of the Photo
	 * and the path of the photo)<br>
	 * -adds a listener to the photosInAlbumListView so that any time it's selected Photo is changed
	 * its info is displayed in the appropriate fields.<br>
	 * -tries to select the Photo in the list corresponding to the selectedPhoto in the SceneTransitionController
	 */
	@FXML
	public void initialize() {
		photosInAlbumListView.setItems(FXCollections.observableArrayList(Photos.transitionController.selectedAlbum.albumPhotos));
		
		photosInAlbumListView.setCellFactory(param -> new ListCell<Photo>() {
            private ImageView imageView = new ImageView();
            @Override
            public void updateItem(Photo photo, boolean empty) {
                super.updateItem(photo, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                	File file = new File(photo.path);
                	Image image = null;
                	
                	if(file.exists()) {
                    	image = new Image(file.toURI().toString(), 200, 200, true, true);
                        imageView.setImage(image);
                        
                        setText(photo.path);
                        setGraphic(imageView);
                	} else {
                		setText(photo.path);
                		setGraphic(null);
                	}
                }
            }
        });
		
		photosInAlbumListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Photo>() {
			@Override
			public void changed(ObservableValue<? extends Photo> observable, Photo oldValue, Photo newValue) {
				Photos.transitionController.selectedPhoto = newValue;
				
				if(Photos.transitionController.selectedPhoto != null) {
					selectedPhotoCaptionTextField.setText(Photos.transitionController.selectedPhoto.caption);
					peopleTaggedInCurrentPhotoListView.setItems(FXCollections.observableArrayList(Photos.transitionController.selectedPhoto.taggedPersons));					
					if(Photos.transitionController.selectedPhoto.locationTag != null) {
						selectedPhotoLocationTagTextField.setText(Photos.transitionController.selectedPhoto.locationTag.toString());	
					} else {
						selectedPhotoLocationTagTextField.setText("");
					}
				} else {
					selectedPhotoCaptionTextField.setText("");
					peopleTaggedInCurrentPhotoListView.setItems(null);
					selectedPhotoLocationTagTextField.setText("");
				}				
				
				if(newValue != null) {
					File file = new File(newValue.path);
	            	Image image = null;
	            	
	            	if(file.exists()) {
	                	image = new Image(file.toURI().toString());
	                    currentPhotoImageView.setImage(image);
	               	} else {
	               		currentPhotoImageView.setImage(null);
	               	}					
				} else {
					currentPhotoImageView.setImage(null);
				}
			}
		});
		
		if(Photos.transitionController.selectedPhoto != null) {
			photosInAlbumListView.getSelectionModel().select(Photos.transitionController.selectedPhoto);
		} else {
			selectedPhotoCaptionTextField.setText("");
			peopleTaggedInCurrentPhotoListView.setItems(null);
			selectedPhotoLocationTagTextField.setText("");
			currentPhotoImageView.setImage(null);
		}
	}
	
	/**
	 * Triggers the AddPhoto view / AddPhotoController, allowing the user
	 * to add a new Photo to the selected Album.
	 */
	public void addNewPhotoAction() {
		Photos.transitionController.transitionFromPhotoManagementToAddPhoto();
	}
	
	/**
	 * Removes the selected Photo from the selected Album. This will fail if no Photo is selected.
	 */
	public void removeSelectedPhotoAction() {
		if(Photos.transitionController.selectedPhoto != null) {
			Alert confirmation = new Alert(AlertType.CONFIRMATION);
			confirmation.initOwner(mainStage); 
			confirmation.setTitle("Remove Photo From Album");
			confirmation.setHeaderText("Are you sure you want to remove this Photo?");
			Optional<ButtonType> result = confirmation.showAndWait();

			if (result.get() == ButtonType.OK){
				if(!Photos.transitionController.selectedAlbum.removePhoto(Photos.transitionController.selectedPhoto.path)) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.initOwner(mainStage); 
					alert.setTitle("Removing Photo Failed!");
					alert.setHeaderText("An error occurred when trying to remove this photo.");
					alert.setContentText("Please try again. If the problem persists, contact your administrator.");
					alert.showAndWait();
				}
				photosInAlbumListView.setItems(FXCollections.observableArrayList(Photos.transitionController.selectedAlbum.albumPhotos));
			}
			
		} else {
			this.displaySelectAPhotoPrompt();
		}
	}
	
	/**
	 * Triggers the CopyPhotoToAlbum view / CopyPhotoToAlbumController, allowing the user
	 * to copy the selected Photo to another Album. This will fail if no Photo is selected.
	 */
	public void copySelectedPhotoToNewAlbumAction() {
		if(Photos.transitionController.selectedPhoto != null) {
			Photos.transitionController.transitionFromPhotoManagementToCopyPhoto();
		} else {
			this.displaySelectAPhotoPrompt();
		}
	}
	
	/**
	 * Triggers the MovePhotoToAlbum view / MovePhotoToAlbumController, allowing the user
	 * to move the selected Photo to another Album. This will fail if no Photo is selected.
	 */
	public void moveSelectedPhotoToNewAlbumAction() {
		if(Photos.transitionController.selectedPhoto != null) {
			Photos.transitionController.transitionFromPhotoManagementToMovePhoto();
		} else {
			this.displaySelectAPhotoPrompt();
		}
	}

	/**
	 * Triggers the ViewPhoto view / ViewPhotoController, allowing the user
	 * to view the selected Photo in more detail. This will fail if no Photo is selected.
	 */
	public void viewSelectedPhotoAction() {
		if(Photos.transitionController.selectedPhoto != null) {
			Photos.transitionController.transitionFromPhotoManagementToViewPhoto();			
		} else {
			this.displaySelectAPhotoPrompt();
		}

	}
	
	/**
	 * Displays a generic error prompt about needing to select a Photo to use a feature.
	 */
	public void displaySelectAPhotoPrompt() {
		Alert alert = new Alert(AlertType.ERROR);
		alert.initOwner(mainStage); 
		alert.setTitle("No Photo Selected!");
		alert.setHeaderText("");
		alert.setContentText("Selecting a Photo is required to perform this action, please select one and try again.");
		alert.showAndWait();
	}
	
	/**
	 * Triggers the CreateAlbum view / CreateAlbumController, allowing the user to
	 * create a new Album with a provided Album name from the list of Photos displayed on this 
	 * screen.
	 */
	public void createNewAlbumAction() {
		Photos.transitionController.transitionFromPhotoManagementToCreateAlbum();
	}
	
	/**
	 * Updates the caption of the selected Photo, this will fail if no Photo is selected.
	 */
	public void updateCaptionAction() {
		if(Photos.transitionController.selectedPhoto != null) {
			Alert confirmation = new Alert(AlertType.CONFIRMATION);
			confirmation.initOwner(mainStage); 
			confirmation.setTitle("Recaption Photo");
			confirmation.setHeaderText("Are you sure you want to recaption this Photo?");
			Optional<ButtonType> result = confirmation.showAndWait();

			if (result.get() == ButtonType.OK){
				Photos.transitionController.selectedPhoto.updateCaption(this.selectedPhotoCaptionTextField.getText());
			}
			
		} else {
			this.displaySelectAPhotoPrompt();
		}
	}
	
	/**
	 * Triggers the AddTag view / AddTagController, allowing the user to
	 * add a Tag to the selected Photo. This will fail if no Photo is selected.
	 */
	public void addTagToSelectedPhotoAction() {
		if(Photos.transitionController.selectedPhoto != null) {
			Photos.transitionController.transitionFromPhotoManagementToAddTag();
		} else {
			this.displaySelectAPhotoPrompt();
		}
	}
	
	/**
	 * Triggers the RemoveTag view / RemoveTagController, allowing the user to
	 * remove a Tag to the selected Photo. This will fail if no Photo is selected.
	 */
	public void removeTagFromSelectedPhotoAction() {
		if(Photos.transitionController.selectedPhoto != null) {
			Photos.transitionController.transitionFromPhotoManagementToRemoveTag();
		} else {
			this.displaySelectAPhotoPrompt();
		}
	}
	
	/**
	 * Selects the previous Photo in the photosInAlbumListView based on the currently selected Photo,
	 * if the first Photo is already selected, no action is taken, if no Photo is selected, 
	 * no action is taken.
	 */
	public void selectPreviousPhotoAction() {
		int currentPhotoIndex = this.photosInAlbumListView.getSelectionModel().getSelectedIndex();
		
		if(currentPhotoIndex > 0) {
			this.photosInAlbumListView.getSelectionModel().select(currentPhotoIndex - 1);			
		}
	}
	
	/**
	 * Selects the next Photo in the photosInAlbumListView based on the currently selected Photo,
	 * if the last Photo is already selected, no action is taken, if no Photo is selected, 
	 * the first Photo is selected.
	 */
	public void selectNextPhotoAction() {
		int currentPhotoIndex = this.photosInAlbumListView.getSelectionModel().getSelectedIndex();
		
		if(currentPhotoIndex < this.photosInAlbumListView.getItems().size()) {
			this.photosInAlbumListView.getSelectionModel().select(currentPhotoIndex + 1);			
		}
	}
	
	/**
	 * Returns the user to the AlbumManagement view / AlbumManagementController.
	 */
	public void returnToAlbumManagementAction() {
		Photos.transitionController.selectedPhoto = null;
		Photos.transitionController.transitionFromPhotoManagementToAlbumManagement();
	}
	
	/**
	 * Log out the current user and return to the Login view / LoginController.
	 */
	public void logoutAction() {
		Photos.transitionController.logout();
	}
}
