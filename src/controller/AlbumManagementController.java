package controller;

import java.util.Optional;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Alert.AlertType;
import model.Album;

/**
 * The controller class for the AlbumManagment view which displays
 * all of the currently logged in User's Albums and allows the user
 * to:<br>
 * -create new Albums<br>
 * -rename the selected existing Album<br>
 * -view the selected existing Album<br>
 * -delete the selected existing Album<br>
 * -search their Photos using a Date range (inclusive)<br>
 * -search their Photos using one or two Tags<br>
 * -log out
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class AlbumManagementController extends PhotosController {
	
	/** The TextField populated by the selected Album's albumName 
	 * when the User selects an Album from the albumListTableView.
	 * This can also be changed manually by the user / entered manually
	 * by the user to correspond to a new name for the selected Album or
	 * to create a new Album with the entered name.*/
	@FXML
	private TextField albumNameTextField;
	
	/** The TableView used to list the current User's Albums and to 
	 * select an Album from to modify / view / delete.*/
	@FXML
	private TableView<Album> albumListTableView;

	/** The Button used to trigger logging the current User out.*/
	@FXML
	private Button logoutButton;
	
	/** The Button used to trigger the creation of a new Album for the 
	 * current User (this also adds it to the albumListTableView.*/
	@FXML
	private Button createNewAlbumButton;
	
	/** The Button used to trigger renaming the selected Album in the
	 * albumListTableView (this occurs by changing its albumName property
	 * to match the value displayed in the albumNameTextField.*/
	@FXML
	private Button renameSelectedAlbumButton;
	
	/** The Button used to trigger viewing the Album selected in the
	 * albumListTableView, transitioning to the PhotoManagement view /
	 * TableManagementController.*/
	@FXML
	private Button viewSelectedAlbumButton;
	
	/** The Button used to remove the Album selected in the albumListTableView
	 * from the selected User's albums.*/
	@FXML
	private Button deleteSelectedAlbumButton;
	
	/** The Button used to trigger searching the current User's Photos based on
	 * a range of Dates.*/
	@FXML
	private Button searchPhotosUsingDateRangeButton;
	
	/** The Button used to trigger searching the current User's Photos based on
	 * one or two Tags.*/
	@FXML
	private Button searchPhotosUsingTagsButton;
	
	/** The TableColumn in the albumListTableView used for displaying the names of the 
	 * current User's Albums.*/
	@FXML
	private TableColumn<Album, String> albumNameColumn;

	/** The TableColumn in the albumListTableView used for displaying the number of images in
	 * the current User's Albums.*/
	@FXML
	private TableColumn<Album, String> numberOfImagesColumn;
	
	/** The TableColumn in the albumListTableView used for displaying the earliest date of
	 * any Photo in each Album belonging to the current User.*/
	@FXML
	private TableColumn<Album, String> earliestDateColumn;

	/** The TableColumn in the albumListTableView used for displaying the latest date of
	 * any Photo in each Album belonging to the current User.*/
	@FXML
	private TableColumn<Album, String> latestDateColumn;
	
	@Override
	public String fxmlPath() { 
		return "../view/AlbumManagement.fxml";	
	}

	@Override
	public String viewTitle() {
		return "Album Management";
	}
	
	/**
	 * On load, populate the albumListTableView with the logged in User's albums,
	 * configure how each of the 4 TableColumns is populated based on each Album in the TableView,
	 * set the albumNameTextField to populate based on the Album selected in the TableView,
	 * if there is an Album selected in transitionController, selects that Album in the albumListTableView. 
	 */
	@FXML
	public void initialize() {
		if(Photos.transitionController.loggedInUser.userAlbums.size() > 0 && Photos.transitionController.loggedInUser.userAlbums != null) {
			albumListTableView.setItems(FXCollections.observableArrayList(Photos.transitionController.loggedInUser.userAlbums));			
		}
		
		albumNameColumn.setCellValueFactory(new PropertyValueFactory<Album, String>("albumName"));
		numberOfImagesColumn.setCellValueFactory(new PropertyValueFactory<Album, String>("numberOfImages"));
		earliestDateColumn.setCellValueFactory(new PropertyValueFactory<Album, String>("earliestDate"));
		latestDateColumn.setCellValueFactory(new PropertyValueFactory<Album, String>("latestDate"));
		
		albumListTableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Album>() {
			@Override
			public void changed(ObservableValue<? extends Album> observable, Album oldValue, Album newValue) {
				if(newValue != null) {
					albumNameTextField.setText(newValue.albumName);					
				}
				Photos.transitionController.selectedAlbum = newValue;
			}
		});
		
		if(Photos.transitionController.selectedAlbum != null && !albumListTableView.getItems().isEmpty()) {
			albumListTableView.getSelectionModel().select(Photos.transitionController.selectedAlbum);
		}
	}
	
	/**
	 * Attempts to create a new Album for the logged in User. This will fail if either of the 
	 * following is true:<br>
	 * -the albumNameTextField is empty<br>
	 * -there already exists an Album with this name for the current User.
	 */
	public void createNewAlbumAction() {
		String potentialNewAlbumName = this.albumNameTextField.getText();
		
		if(potentialNewAlbumName.isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Adding Album!");
			alert.setHeaderText("No Album Name Entered!");
			alert.setContentText("An album name is needed to create an album, please enter one in the Album Name text field and try again.");
			alert.showAndWait();
			return;
		}
		
		if(Photos.transitionController.loggedInUser.addAlbum(potentialNewAlbumName)) {
			albumListTableView.setItems(FXCollections.observableArrayList(Photos.transitionController.loggedInUser.userAlbums));
			albumListTableView.getSelectionModel().select(Photos.transitionController.loggedInUser.getAlbum(potentialNewAlbumName));
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Adding Album!");
			alert.setHeaderText("Invalid Album Name!");
			alert.setContentText("The album name " + potentialNewAlbumName + " is invalid, please try a different album name.");
			alert.showAndWait();
		}
	}
	
	/**
	 * Tries to rename the Album selected in the albumListTableView to match the album name
	 * in the albumNameTextField. This will fail if either of the following is true:<br>
	 * -the albumNameTextField is empty<br>
	 * -there is already an Album for the current User with the name in the albumListTableView
	 */
	public void renameSelectedAlbumAction() {
		if(!albumNameTextField.getText().isEmpty()) {
			if(!Photos.transitionController.loggedInUser.albumExistsWithName(albumNameTextField.getText()) ) {
				Alert confirmation = new Alert(AlertType.CONFIRMATION);
				confirmation.initOwner(mainStage); 
				confirmation.setTitle("Rename Album");
				confirmation.setHeaderText("Are you sure you want to rename this Album?");
				Optional<ButtonType> result = confirmation.showAndWait();

				if (result.get() == ButtonType.OK){
					Photos.transitionController.selectedAlbum.setAlbumName(albumNameTextField.getText());
					albumListTableView.setItems(FXCollections.observableArrayList(Photos.transitionController.loggedInUser.userAlbums));
					albumListTableView.refresh();				
				}
				
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(mainStage); 
				alert.setTitle("Error Renaming Album!");
				alert.setHeaderText("Album Name already exists!");
				alert.setContentText("Please enter a valid Album name and try again.");
				alert.showAndWait();
			}
			
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Renaming Album!");
			alert.setHeaderText("Album Name must not be blank!");
			alert.setContentText("Please enter a valid Album name and try again.");
			alert.showAndWait();
		}
	}
	
	/**
	 * Transitions to the PhotoManagement view / PhotoManagementController if an Album
	 * is selected.
	 */
	public void viewSelectedAlbumAction() {
		if(Photos.transitionController.selectedAlbum != null) {
			Photos.transitionController.transitionFromAlbumManagementToPhotoManagement();			
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Viewing Album!");
			alert.setHeaderText("No Album selected!");
			alert.setContentText("Please an Album and try again.");
			alert.showAndWait();
		}

	}
	
	/**
	 * If an Album is selected, deletes it from the list of Albums belonging to the logged in User.
	 */
	public void deleteSelectedAlbumAction() {		
		if(Photos.transitionController.selectedAlbum != null) {
			Alert confirmation = new Alert(AlertType.CONFIRMATION);
			confirmation.initOwner(mainStage); 
			confirmation.setTitle("Delete Album");
			confirmation.setHeaderText("Are you sure you want to delete this Album?");
			Optional<ButtonType> result = confirmation.showAndWait();

			if (result.get() == ButtonType.OK){
				Photos.transitionController.loggedInUser.removeAlbum(Photos.transitionController.selectedAlbum.albumName);			
			}
			
		} else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Deleting Album!");
			alert.setHeaderText("No Album selected!");
			alert.setContentText("Please an Album and try again.");
			alert.showAndWait();
		}
		
		albumListTableView.setItems(FXCollections.observableArrayList(Photos.transitionController.loggedInUser.userAlbums));
		
		if(!albumListTableView.getSelectionModel().isEmpty()) {
			Photos.transitionController.selectedAlbum = albumListTableView.getSelectionModel().getSelectedItem();			
		}
	}
	
	/**
	 * Trigger a Date search, navigating to the ConstructDateRangeSearch view / 
	 * ConstructDateRangeSearchController.
	 */
	public void searchPhotosUsingDateRangeAction() {
		Photos.transitionController.transitionFromAlbumManagementToDateRangeSearch();
	}
	
	/**
	 * Trigger a Tag search, navigating to the ConstructTagSearch view / ConstructTagSearchController.
	 */
	public void searchPhotosUsingTagsAction() {
		Photos.transitionController.transitionFromAlbumManagementToTagSearch();
	}
	
	/**
	 * Log out the current user and return to the Login view / LoginController.
	 */
	public void logoutAction() {
		SceneTransitionController controller = new SceneTransitionController();
		controller.logout();
	}
}
