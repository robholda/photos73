package controller;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Alert.AlertType;
import model.Album;
import model.Photo;

/**
 * The controller class for the CopyPhotoToAlbum view. Allows the 
 * User to select an Album to copy the selected Photo to.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class CopyPhotoToAlbumController extends PhotosController {
	
	/** The ComboBox where all of the logged in User's Albums are listed.*/
	@FXML
	private ComboBox<Album> userAlbumsComboBox;
	
	/** The Button that triggers actually copying the Photo to the User selected Album.*/
	@FXML
	private Button copyPhotoButton;
	
	/** The Button that cancels copying the Photo, returning the User to the PhotoManagement view /
	 * PhotoManagementController. */
	@FXML
	private Button cancelButton;
	
	@Override
	public String fxmlPath() { 
		return "../view/CopyPhotoToAlbum.fxml";
	}

	@Override
	public String viewTitle() {
		return "Copy Photo To Album";
	}
	
	/**
	 * On load, populate the userAlbumsComboBox with the logged in User's Albums and select the
	 * Album the selected Photo is currently a part of.
	 */
	@FXML
	public void initialize() {
		userAlbumsComboBox.setItems(FXCollections.observableArrayList(Photos.transitionController.loggedInUser.userAlbums));
		userAlbumsComboBox.getSelectionModel().select(Photos.transitionController.selectedAlbum);
	}
	
	/**
	 * Copies the selected Photo to the Album specified by the User. This will fail if either of
	 * the following is true:<br>
	 * -The selected Album is the same Album the selected Photo is currently a part of<br>
	 * -The selected Album already contains the selected Photo
	 */
	public void copyPhotoAction() {
		if(userAlbumsComboBox.getSelectionModel().getSelectedItem().equals(Photos.transitionController.selectedAlbum)) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Copying Photo!");
			alert.setHeaderText("Cannot copy a Photo to the Album you are copying it from!");
			alert.setContentText("Please select a different Album and try again.");
			alert.showAndWait();
		} else {
			if(!Photos.transitionController.loggedInUser.getAlbum(userAlbumsComboBox.getSelectionModel().getSelectedItem().albumName).photoExistsInAlbumWithName(Photos.transitionController.selectedPhoto.path)) {
				if(Photos.transitionController.loggedInUser.getAlbum(userAlbumsComboBox.getSelectionModel().getSelectedItem().albumName).addPhoto(new Photo(Photos.transitionController.selectedPhoto))) {
					Photos.transitionController.transitionFromCopyPhotoToPhotoManagement();
				}	
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(mainStage); 
				alert.setTitle("Error Copying Photo!");
				alert.setHeaderText("Cannot copy a Photo to an Album the same Photo already exists in!");
				alert.setContentText("This Album already contains the Photo you are copying. Please select a different Album and try again.");
				alert.showAndWait();
			}
		}
	}
	
	/**
	 * Backs out of the CopyPhotoToAlbum view / CopyPhotoToAlbumController, 
	 * returning the User to the PhotoManagement view / PhotoManagementController.
	 */
	public void cancelAction() {
		Photos.transitionController.transitionFromCopyPhotoToPhotoManagement();
	}
}
