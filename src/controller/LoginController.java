package controller;

import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

/**
 * The controller class for the Login view. Allows the 
 * User to log in as one of the existing Users or as 'stock'
 * or 'admin'.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class LoginController extends PhotosController {

	/** The admin userName*/
	String adminUsername = "admin";
	
	/** The stock userName*/
	String stockUsername = "stock";
	
	/** The TextField where the User can provide their username*/
	@FXML
	private TextField usernameTextField;

	/** The Button that triggers actually logging in.*/
	@FXML
	private Button loginButton;
	
	@Override
	public String fxmlPath() { 
		return "../view/Login.fxml";
	}

	@Override
	public String viewTitle() {
		return "Login";
	}
	
	/**
	 * Actually tries to log in. This will fail if the username provided in the
	 * usernameTextField is none of the following:<br>
	 * -admin<br>
	 * -stock<br>
	 * -the username of any existing user
	 */
	public void loginAction() {
		
		if(usernameTextField.getText().equalsIgnoreCase(adminUsername)) {
			Photos.transitionController.transitionFromLoginToAdmin();			
		} else {
			if(this.validateUsername(usernameTextField.getText())) {
				Photos.transitionController.transitionFromLoginToAlbumManagementWithUserName(usernameTextField.getText());
			} else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(mainStage); 
				alert.setTitle("Login Error!");
				alert.setHeaderText("Invalid Username!");
				alert.setContentText("Please confirm your username and try again. \n\nIf you continue to experience issues, contact your administrator.");
				alert.showAndWait();
			}
		}
	}
	
	/**
	 * Validate the username String provided in the parameter.
	 * 
	 * @param username The username String we are validating.
	 * @return true if the username is either the stock username or if the username matches
	 * the username of one of the existing Users, false otherwise.
	 */
	public boolean validateUsername(String username) {
		if(username.equalsIgnoreCase(stockUsername)) {
			return true;
		} else {
			if(Photos.transitionController.userExistsWithName(username)) {
				return true;
			}
			
			return false;
		}
	}
}
