package controller;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.*;
import java.util.Optional; 

/**
 * The main class of our photos app. Contains both the definitions of
 * the path and fileName to save and load our state to as well as the
 * logic to save and load the app.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 *
 */
public class Photos extends Application implements Serializable {

	/** The serial version id for the Photos class. */
	private static final long serialVersionUID = -3664962511677428815L;
	
	/** The subdirectory within the project where we save the state of the photos app.*/
	public static final String saveDir = "data";
	
	/** The filename to save the state of the photos app to.*/
	public static final String saveFile = "photos.dat"; 
	
	/** The controller where we store the state of our app while it is active 
	 * and where we trigger the transition from scene to scene.*/
	public static SceneTransitionController transitionController;
	
	/**
	 * The main function of our photos app. 
	 * 
	 * @param args This String array of arguments is passed to the launch
	 * function of the Application class.
	 */
	public static void main(String[] args) {
		launch(args);
	}

	/**
	 * The EventHandler for adding a confirmation on quit.
	 */
	private EventHandler<WindowEvent> confirmCloseEventHandler = event -> {
		Alert confirmation = new Alert(AlertType.CONFIRMATION);
		confirmation.initOwner(SceneTransitionController.mainStage); 
		confirmation.setTitle("Quit");
		confirmation.setHeaderText("Are you sure you want to Quit?");
		Optional<ButtonType> result = confirmation.showAndWait();

		if (result.get() != ButtonType.OK){
			event.consume();
		}
	};
	
	/**
	 * Starts the application, if a previously stored save can be loaded
	 * loads that state if not, sets up the default application state.
	 */
	@Override
	public void start(Stage stage) throws Exception {
		try {
		transitionController = Photos.readAppState();
		} catch(IOException e) {

		}
		
		if(transitionController == null) {
			transitionController = new SceneTransitionController();
			transitionController.initialize();
		}
		
		transitionController.setStage(stage);
		
		SceneTransitionController.mainStage.setOnCloseRequest(confirmCloseEventHandler);
		
		transitionController.loadLogin();

	}
	
	/**
	 * Ends the application, attempting to save the current application state
	 * and reporting any errors it encounters along the way.
	 */
	@Override
	public void stop(){
		
		    try {
				writeAppState(Photos.transitionController);
			} catch (IOException e) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initOwner(SceneTransitionController.mainStage); 
				alert.setTitle("Error Saving Data!");
				alert.setHeaderText("An error has occurred while saving your data!\n Error info is listed below:");
				alert.setContentText(e.toString());
				alert.showAndWait();
			}
	}
	
	/**
	 * Actually write the current application state to our data file. 
	 * 
	 * @param controller The SceneTransitionController where we store our application's state
	 * while running.
	 * 
	 * @throws IOException An error was encountered when trying to write the application's state for storage.
	 */
	public static void writeAppState (SceneTransitionController controller) throws IOException {
		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(saveDir + File.separator + saveFile));
		oos.writeObject(controller);
		oos.close();
	}
	
	/**
	 * Actually read the saved application state from our data file.
	 * 
	 * @return The SceneTransitionController we've loaded from our data file.
	 * @throws IOException An error was encountered when trying to read the application's state from storage.
	 * @throws ClassNotFoundException An error was encountered when trying to parse our application's state from storage. 
	 */
	public static SceneTransitionController readAppState() throws IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream(saveDir + File.separator + saveFile));
				SceneTransitionController controller = (SceneTransitionController)ois.readObject();
				ois.close();
				return controller;
		}
}
