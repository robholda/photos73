package controller;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import model.User;

/**
 * The controller for the UserManagement view which handles
 * adding and deleting Users.
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 */
public class AdminController extends PhotosController {
	/** The ListView that displays the currently existing Users 
	 * (not including the 'stock' and 'admin' users which may not be deleted.*/
	@FXML
	private ListView<User> userListView;
	
	/** The TextField within which the user may enter a username to add or delete.
	 * Populates with the username of the selected user in the UserListView if applicable.*/
	@FXML
	private TextField usernameTextField;
	
	/** The Button which triggers logging the admin out.*/
	@FXML
	private Button logoutButton;
	
	/** The Button which triggers adding a new User with the username listed in the
	 * usernameTextField (if that username is valid).*/
	@FXML
	private Button addUserButton;

	/** The Button which triggers deleting an existing User with the username listed in the
	 * usernameTextField (if that User exists).*/
	@FXML
	private Button deleteUserButton;
	
	@Override
	public String fxmlPath() { 
		return "../view/Admin.fxml";	
	}

	@Override
	public String viewTitle() {
		return "User Management";
	}
	
	/**
	 * Populates the userListView with the usernames of any existing Users who are neither the
	 * 'stock' user nor the 'admin' user and registers a listener to update the usernameTextField's
	 * text whenever one of the userListView's items is selected.
	 */
	@FXML
	public void initialize() {
		userListView.setItems(FXCollections.observableArrayList(Photos.transitionController.nonAdminUsers));
		
		userListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<User>() {
			@Override
			public void changed(ObservableValue<? extends User> observable, User oldValue, User newValue) {
				if(newValue != null) {
					usernameTextField.setText(newValue.userName);					
				}
			}
		});
	}
	
	/**
	 * Determines whether the username listed in the usernameTextField is valid
	 * to be added as a new User. In order to be added, the username must:<br>
	 * -not be empty string<br>
	 * -not be 'stock'<br>
	 * -not be 'admin'<br>
	 * -not already be taken by another user.<br><br>
	 * If the username passes all these tests,
	 * a new User object is created and added to the list of non-admin / stock Users.
	 */
	public void addUserAction() {
		String possibleUserName = this.usernameTextField.getText();
		if(!possibleUserName.isEmpty() && 
				!Photos.transitionController.userExistsWithName(possibleUserName) && 
				!possibleUserName.equalsIgnoreCase("admin") && 
				!possibleUserName.equalsIgnoreCase("stock")) {
			User newUser = new User(possibleUserName);
			Photos.transitionController.nonAdminUsers.add(newUser);
			userListView.setItems(FXCollections.observableArrayList(Photos.transitionController.nonAdminUsers));
		} else {	
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Adding User!");
			alert.setHeaderText("Invalid Username!");
			alert.setContentText("The username " + possibleUserName + " is invalid, please try a different username.");
			alert.showAndWait();
		}
		
		sortUsers(Photos.transitionController.nonAdminUsers);
	}
	
	/**
	 * Sorts the provided list of User objects alphabetically based on 
	 * their usernames in ascending order. 
	 * 
	 * @param obsList The list of User objects to sort.
	 */
	private void sortUsers(ArrayList <User> obsList) {
		obsList.sort(new Comparator<User>() {
			@Override
			public int compare(User item, User secondItem) {
				return item.userName.compareTo(secondItem.userName);
			}
		});	

		userListView.setItems(FXCollections.observableArrayList(obsList)); 
	}
	
	/**
	 * Determines whether the username listed in the usernameTextField corresponds to a
	 * valid existing non-stock, non-admin User. In order for deletion to be successful:<br>
	 * -the username must not be empty string<br>
	 * -the username must correspond to an existing user who is neither 'stock' nor 'admin'<br>
	 * -'admin' must confirm they wish to delete the selected User<br><br>
	 * If all of the above occur,
	 * a the User object corresponding to the selected username is deleted.
	 */
	public void deleteUserAction() {
		if(Photos.transitionController.nonAdminUsers.size() < 1) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Deleting User!");
			alert.setHeaderText("No user to delete!");
			alert.setContentText("There are currently no users to delete.");
			alert.showAndWait();
			return;
		}
		
		String possibleUserName = this.usernameTextField.getText();
		if(Photos.transitionController.userExistsWithName(possibleUserName)) {
			Alert userDeletionConfirmation = new Alert(AlertType.CONFIRMATION);
			userDeletionConfirmation.initOwner(mainStage); 
			userDeletionConfirmation.setTitle("Delete User");
			userDeletionConfirmation.setHeaderText("Are you sure you want to delete this user?");
			userDeletionConfirmation.setContentText("Doing so will delete all their albums from the program (photos will still remain on your computer but will need to be re-added to the program).");
			Optional<ButtonType> result = userDeletionConfirmation.showAndWait();

			if (result.get() == ButtonType.OK){
				Photos.transitionController.deleteUser(possibleUserName);	
				userListView.setItems(FXCollections.observableArrayList(Photos.transitionController.nonAdminUsers));
			}
		} else {	
			Alert alert = new Alert(AlertType.ERROR);
			alert.initOwner(mainStage); 
			alert.setTitle("Error Deleting User!");
			alert.setHeaderText("Invalid Username!");
			alert.setContentText("The username " + possibleUserName + " doesn't exist, please confirm the username of the user you wish to delete and try again.");
			alert.showAndWait();
		}
	
	}
	
	/**
	 * Log out the 'admin' user and return to the Login view / LoginController.
	 */
	public void logoutAction() {
//		Alert logoutConfirmation = new Alert(Alert.AlertType.CONFIRMATION);
//		logoutConfirmation.setTitle("Log Out");
//		logoutConfirmation.setHeaderText("Are you sure you want to log out?");
//		
//		ButtonType okButton = new ButtonType("Log Out", ButtonBar.ButtonData.OK_DONE);
//		ButtonType noButton = new ButtonType("Cancel", ButtonBar.ButtonData.NO);
//
//		logoutConfirmation.getButtonTypes().setAll(noButton, okButton);
//		
//		Optional<ButtonType> logoutConfirmationResult = logoutConfirmation.showAndWait();
//
//		if(logoutConfirmationResult.get().equals( ButtonType.OK) ) {
//        	System.out.println("test");
			
    		Photos.transitionController.logout();
//		}
		
	}
}
