package model;

import java.util.ArrayList;
import java.util.Date;

import controller.SceneTransitionController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * The Photo class contains model properties for a Photo's path, caption, imageDate,
 * and location and person Tags. It also contains the various functions
 * required of the Photo model and various ways to manipulate the Photo's
 * members and the Tags it is tagged with. 
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 *
 */
public class Photo implements Serializable {
	
	/** The serial version id for the Photo class. */
	private static final long serialVersionUID = 8175910263040036971L;
	
	/** The path of this Photo (this is the absolute path of the Photo,
	 * including the file's name and extension). This is used to uniquely
	 * identify the Photo within an Album.*/
	public String path;
	
	/** The caption applied to the Photo*/
	public String caption;
	
	/** The image Date of the Photo, this is set using the file's dateModified property.
	 * Used when searching for Photos by Date.*/
	public Date imageDate;
	
	/** The location Tag this Photo is tagged with.*/
	public Tag locationTag;
	
	/** The list of people this Photo is Tagged with.*/
	public ArrayList<Tag> taggedPersons;
	
	/**
	 * Create a new Photo with a path value matching the provided String value
	 * and no tags.
	 * 
	 * @param pathString The String to set the path member of this Photo to. This is
	 * also used to grab the lastModified value of this Photo.
	 */
	public Photo(String pathString) {
		this.path = pathString;
		File test = new File(this.path);
		imageDate = new Date(test.lastModified());
		this.taggedPersons = new ArrayList<Tag>();
	}
	
	/**
	 * Get only the name and extension of this Photo.<br>
	 * ex. if a photo is located at /dir1/dir2/dir3/photo.png, returns photo.png
	 * 
	 * @return A String composed of only this Photo's filename and extension.
	 */
	public String getOnlyFileNameAndExtension() {
		int lastSlashIndex = this.path.lastIndexOf('/') + 1;
		return this.path.substring(lastSlashIndex);
	}
	
	/**
	 * Create a new Photo by copying the passed Photo and all of its info.
	 * 
	 * @param photoToCopy The Photo we are copying.
	 */
	public Photo(Photo photoToCopy) {
		this.path = photoToCopy.path;
		this.caption = photoToCopy.caption;
		this.imageDate = photoToCopy.imageDate;
		if(photoToCopy.locationTag != null) {
			this.locationTag = new Tag(photoToCopy.locationTag);			
		}
		
		this.taggedPersons = new ArrayList<Tag>();
		for(Tag currentTag : photoToCopy.taggedPersons) {
			this.taggedPersons.add(new Tag(currentTag));
		}
	}
	
	/**
	 * Removes the provided tag, if it is a location Tag, sets locationTag to null,
	 * if it is a person Tag, removes that person Tag from taggedPersons.
	 * 
	 * @param tagToRemove The Tag we are attempting to remove from this Photo.
	 */
	public void removeTag(Tag tagToRemove) {
		if(tagToRemove.tagType.equalsIgnoreCase(Tag.locationTagString)) {
			locationTag = null;
		} else {
			taggedPersons.remove(tagToRemove);
		}
	}
	
	/**
	 * Get a list of all the Tags applied to this Photo.
	 * 
	 * @return A list of all the tags applied to this Photo including the locationTag
	 * if there is one and all of the taggedPersons Tags.
	 */
	public ObservableList<Tag> getAllTags() {
		ObservableList<Tag> allTags = FXCollections.observableArrayList();
		
		if(locationTag != null) {
			allTags.add(locationTag);			
		}
		allTags.addAll(FXCollections.observableArrayList(taggedPersons));
		
		return allTags;
	}
	
	/**
	 * Parses the imageDate value of this Photo into a String.
	 * 
	 * @return The parsed String value of the imageDate member of this Photo.
	 */
	public String imageDateString() {
		DateFormat dateFormatter = new SimpleDateFormat(SceneTransitionController.dateFormat);
		
		return dateFormatter.format(this.imageDate);
	}
	
	/**
	 * Adds a location Tag with the String value provided.
	 * 
	 * @param locationTagValue The String value to use when creating a location Tag to
	 * apply to this Photo.
	 */
	public void addLocationTag(String locationTagValue) {
		this.locationTag = Tag.LocationTag(locationTagValue);
	}

	/**
	 * Adds a person Tag with the String value provided.
	 * 
	 * @param personTagValue The String value to use when creating a person Tag to
	 * apply to this Photo.
	 */
	public void addPersonTag(String personTagValue) {
		
		this.taggedPersons.add(Tag.PersonTag(personTagValue));
	}
	
	/**
	 * Determine whether this Photo has a location Tag.
	 * 
	 * @return true if this Photo is tagged with a location Tag, false otherwise.
	 */
	public boolean hasLocationTag() {
		return this.locationTag != null;
	}
	
	/**
	 * Update the caption member of this Photo using the String value provided.
	 * 
	 * @param newCaption The String value to update the caption member of this Photo to.
	 */
	public void updateCaption(String newCaption) {
		this.caption = newCaption;
	}
	
	/**
	 * Determine if this Photo is tagged with the passed Tag.
	 * 
	 * @param tagToCheckAgainst The Tag to check this Photo's Tags for.
	 * @return true if this Photo is tagged with a Tag matching the passed Tag, otherwise false.
	 */
	public boolean hasTag(Tag tagToCheckAgainst) {
		if(tagToCheckAgainst == null) {
			return false;
		}
		
		if(tagToCheckAgainst.tagType.equalsIgnoreCase(Tag.locationTagString)) {
			return (locationTag != null && locationTag.tagValue.equalsIgnoreCase(tagToCheckAgainst.tagValue)); 
		} else {
			for(Tag currentTag : taggedPersons) {
				if(currentTag.tagValue.equalsIgnoreCase(tagToCheckAgainst.tagValue)) {
					return true;
				}
			}
			return false;
		}
	}
	
	/**
	 * Determine if this Photo's imageDate is before or equal to the provided Date.
	 * 
	 * @param dateToCheckAgainst The Date to check this Photo's imageDate against.
	 * @return true if this Photo's imageDate is before or equal to the provided imageDate. 
	 */
	public boolean isBeforeDate(Date dateToCheckAgainst) {
		return imageDate.before(dateToCheckAgainst) || imageDate.equals(dateToCheckAgainst);
	}

	/**
	 * Determine if this Photo's imageDate is after or equal to the provided Date.
	 * 
	 * @param dateToCheckAgainst The Date to check this Photo's imageDate against.
	 * @return true if this Photo's imageDate is after or equal to the provided imageDate. 
	 */
	public boolean isAfterDate(Date dateToCheckAgainst) {
		return imageDate.after(dateToCheckAgainst) || imageDate.equals(dateToCheckAgainst);
	}
	
	/**
	 * Determines if this Photo's imageDate is between the provided Dates. Either date can be
	 * null but not both. If startDate is null, determines if imageDate is before/equal to endDate.
	 * If endDate is null, determines if imageDate is after/equal to startDate.
	 * 
	 * @param startDate The starting Date of the date range to compare against.
	 * @param endDate The ending Date of the date range to compare against.
	 * @return true if the endDate of this Photo falls within the range described by the startDate
	 * and endDate provided (inclusive). <br> If startDate is null, true if imageDate is before endDate, false otherwise.<br>
	 * If endDate is null, true if imageDate is after startDate, false otherwise.
	 */
	public boolean isBetweenDates(Date startDate, Date endDate) {
		if(startDate == null) {
			return isBeforeDate(endDate);
		}
		
		if(endDate == null) {
			return isAfterDate(startDate);
		}
		
		return isAfterDate(startDate) && isBeforeDate(endDate);
	}
	
	/**
	 * Determine if this Photo is tagged with a person Tag described by the name provided. 
	 * 
	 * @param personName The String value of the person Tag we're checking against.
	 * @return true if this Photo is tagged with a person Tag corresponding to the provided String value.
	 */
	public boolean personIsAlreadyTagged(String personName) {
		for(Tag currentTag : this.taggedPersons) {
			if(currentTag.tagValue.equalsIgnoreCase(personName)) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns the path of this Photo.
	 */
	public String toString() {
		return this.path;
	}
	
	/**
	 * Determines if this Photo is equal to the passed Photo by comparing their path members.
	 * 
	 * @param photoToCheck The Photo object to check against for equality.
	 * @return true if the passed Photo's path member is equal to this Photo's path member.
	 */
	public boolean equals(Photo photoToCheck) {
		return this.path.equalsIgnoreCase(photoToCheck.path);
	}
}
