package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import controller.Photos;

/**
 * The User class contains model properties for a User's userName and the
 * Albums belonging to them. It also contains the various functions required 
 * of the User model for displaying and manipulating its userName and its list of
 * Albums. 
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 *
 */
public class User implements Serializable {

	/** The serial version id for the User class. */
	private static final long serialVersionUID = -849746097831194774L;
	
	/** The list of Albums belonging to this User.*/
	public ArrayList<Album> userAlbums;
	
	/** The String userName used to uniquely define this User.*/
	public String userName;

	/** 
	 * Create a new User with a userName matching the provided String and
	 * an empty list of empty Albums.
	 * 
	 * @param name The String value to set the new User's userName member to.
	 */
	public User(String name) {
		userName = name;
		userAlbums = new ArrayList<Album>();
	}
	
	/**
	 * Modify this User's userName member to match the provided String value.
	 * 
	 * @param newName The String value to update this User's userName to.
	 */
	public void renameUser(String newName) {
		userName = newName;
	}
	
	/**
	 * Create a new Album from results of a search of all this User's Albums and Photos
	 * which are tagged with both Tags provided. If a Photo appears in two or more Albums,
	 * only the first occurrence will be added. 
	 * 
	 * @param firstTag The first Tag to check against when filtering this User's photos. May not be null.
	 * @param secondTag The second Tag to check against when filtering this User's photos. May be null.
	 * @return Returns a new Album composed of all this User's photos which are tagged with
	 * both provided Tags. 
	 */
	public Album createAlbumFromBothTagsSearchCriteria(Tag firstTag, Tag secondTag) {
		Album tempAlbum = new Album("Search Results");
		
		for(Album currentAlbum : userAlbums) {
			tempAlbum.addPhotos(currentAlbum.photosWithBothTags(firstTag, secondTag));
		}
		
		return tempAlbum;
	}
	
	/**
	 * Create a new Album from results of a search of all this User's Albums and Photos
	 * which are tagged with either of the provided Tags. If a Photo appears in two or more Albums,
	 * only the first occurrence will be added. 
	 * 
	 * @param firstTag The first Tag to check against when filtering this User's photos. May not be null.
	 * @param secondTag The second Tag to check against when filtering this User's photos. May be null.
	 * @return Returns a new Album composed of all this User's photos which are tagged with
	 * either of the provided Tags. 
	 */
	public Album createAlbumFromEitherTagsSearchCriteria(Tag firstTag, Tag secondTag) {
		Album tempAlbum = new Album("Search Results");
		
		for(Album currentAlbum : userAlbums) {
			tempAlbum.addPhotos(currentAlbum.photosWithEitherTag(firstTag, secondTag));
		}
		
		return tempAlbum;
	}	
	
	/**
	 * Create a new Album from results of a search of all this User's Albums and Photos
	 * which have imageDates that fall between the provided Dates (inclusive). If a Photo appears in 
	 * two or more Albums, only the first occurrence will be added. Either Date may be null but one must
	 * not be null.
	 * 
	 * @param startDate The starting Date of the date range to filter this User's Photos by. May be null.
	 * @param endDate The ending Date of the date range to filter this User's Photos by. May be null.
	 * @return Returns a new Album composed of all this User's photos which fall within the provided
	 * Date range (inclusive). 
	 */
	public Album createAlbumFromDateRangeSearchCriteria(Date startDate, Date endDate) {
		Album tempAlbum = new Album("Search Results");
		
		for(Album currentAlbum : userAlbums) {
			tempAlbum.addPhotos(currentAlbum.photosBetweenDates(startDate, endDate));
		}
		
		return tempAlbum;
	}
	
	/**
	 * Attempt to add the provided Album to this User's userAlbums.
	 * This will fail if another album with the same albumName already 
	 * exists for this User.
	 * 
	 * @param album The Album we are attempting to add.
	 * @return true if the Album is successfully added, false otherwise.
	 */
	public boolean addAlbum(Album album) {
		if(!albumExistsWithName(album.albumName)) {
			this.userAlbums.add(album);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Attempt to add a new empty Album to this User's userAlbums with
	 * the provided String albumName. This will fail if another album with
	 * the same albumName already exists for this User.
	 * 
	 * @param albumName The String value we are setting the new Album's albumName to.
	 * @return true if the Album is successfully added, false otherwise.
	 */
	public boolean addAlbum(String albumName) {
		if(!albumExistsWithName(albumName)) {
			Album newAlbum = new Album(albumName);
			this.userAlbums.add(newAlbum);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Gets the Album belonging to this User with an albumName matching the 
	 * provided String value or null if there is no matching Album. 
	 * 
	 * @param albumName The String value used to identify the Album we are searching for.
	 * @return The Album identified by the provided String value and belonging to this User.
	 */
	public Album getAlbum(String albumName) {
		for(Album currentAlbum : this.userAlbums) {
			if(currentAlbum.albumName.equalsIgnoreCase(albumName)) {
				return currentAlbum;
			}
		}
		
		return null;
	}
	
	/**
	 * Attempts to remove this User's Album identified by the provided albumName String
	 * if one exists.
	 * 
	 * @param albumName The String value to check this User's Albums' albumName members against.
	 * @return true if the Album was successfully removed, false otherwise.
	 */
	public boolean removeAlbum(String albumName) {
		if(albumExistsWithName(albumName)) {
			this.userAlbums.remove(this.getAlbum(albumName));
			Photos.transitionController.selectedAlbum = null;
			Photos.transitionController.selectedPhoto = null;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Returns the userName of this User.
	 */
	public String toString() {
		return this.userName;
	}
	
	/**
	 * Determines if this User has an Album identified by the provided String value.
	 * 
	 * @param albumName The String value to check this User's Albums' albumName members against.
	 * @return true if this User has an Album identified by the provided String value, false otherwise.
	 */
	public boolean albumExistsWithName(String albumName) {
		for(Album currentAlbum : this.userAlbums) {
			if(currentAlbum.albumName.equalsIgnoreCase(albumName)) {
				return true;
			}
		}
		
		return false;
	}
}
