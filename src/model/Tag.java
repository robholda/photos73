package model;

import java.io.Serializable;

/**
 * The Tag class contains model properties for a Tag's type and value. 
 * It also contains the various functions required of the Tag model
 * for displaying its data and comparing it against other Tags. 
 * 
 * @author Rob Holda
 * @author Nick LaBoy
 *
 */
public class Tag implements Serializable {
	/** The serial version id for the Tag class. */
	private static final long serialVersionUID = -8295405785961953962L;
	
	/** The String used to define the tagType of location Tags.*/
	public static String locationTagString = "Location";
	
	/** The String used to define the tagType of person Tags.*/
	public static String personTagString = "Person";
	
	/** The String value of this tag. Used with tagType to uniquely identify this Tag
	 * on each Photo. Also used when searching by Tag.*/
	public String tagValue;
	
	/** The String type of this tag. Used with tagValue to uniquely identify this Tag
	 * on each Photo.  Also used when searching by Tag.*/
	public String tagType;
	
	/**
	 * Create a new location Tag with the provided String tagValue.
	 * 
	 * @param value The String value to set the new location Tag's tagValue to during creation.
	 * @return The new location Tag.
	 */
	public static Tag LocationTag(String value) {
		return new Tag(locationTagString, value);
	}

	/**
	 * Create a new person Tag with the provided String tagValue.
	 * 
	 * @param value The String value to set the new person Tag's tagValue to during creation.
	 * @return The new person Tag.
	 */
	public static Tag PersonTag(String value) {
		return new Tag(personTagString, value);
	}
	
	/**
	 * Create a new Tag of the provided String value for its type with the provided 
	 * String value for it's value
	 * 
	 * @param type The String value to set this Tag's tagType member to.
	 * @param value The String value to set this Tag's tagValue member to.
	 */
	public Tag(String type, String value) {
		this.tagType = type;
		this.tagValue = value;
	}
	
	/**
	 * Create a new Tag as a copy of the provided Tag.
	 * 
	 * @param tag The tag to copy when creating this Tag.
	 */
	public Tag (Tag tag) {
		this.tagType = tag.tagType;
		this.tagValue = tag.tagValue;
	}
	
	/**
	 * Build the String used to display this Tag in the RemoveTag view.
	 * This is composed of the concatenation of the following:<br>
	 * -This Tag's tagType<br>
	 * -An arrow formed by a hyphen character and a greater-than character<br>
	 * -This Tag's tagValue
	 * 
	 * @return This Tag's tagType and tagValue in the following format: [tagType][arrowDescribedAbove][tagValue]
	 */
	public String fullToString() {
		return this.tagType + "->" + this.tagValue;
	}
	
	/**
	 * Returns the tagValue of this Tag.
	 */
	public String toString() {
		return this.tagValue;
	}
	
	/**
	 * Checks this Tag for equality against the provided Tag based upon both it's tagType and tagValue.
	 * 
	 * @param tagToCompareAgainst The Tag to check this Tag against for equality.
	 * @return true if and only if both this Tag's tagType and tagValue match the provided Tag's tagType and tagValue respectively. 
	 */
	public boolean equals(Tag tagToCompareAgainst) {
		return this.tagType.equalsIgnoreCase(tagToCompareAgainst.tagType) && this.tagValue.equalsIgnoreCase(tagToCompareAgainst.tagValue);
	}
}
